﻿using UnityEngine;
using System.Collections;

public class Schwerkraft : MonoBehaviour {

    public bool fallDown = true;

    public Transform Raumschiff;
    public double Masse;

    private Rigidbody RaumschiffRigidBody;
    private double G = 6.667408 *  Mathf.Pow(10, -11);

	// Use this for initialization
	void Start () {
        Debug.Log("start");

        RaumschiffRigidBody = Raumschiff.GetComponent<Rigidbody>();

        Debug.Log(RaumschiffRigidBody);

        RaumschiffRigidBody.velocity = new Vector3(0, 50, 0);

        StartCoroutine("FallDown");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator FallDown()
    {
        while (fallDown)
        {
            Vector3 forceVect = this.GetComponent<Transform>().localPosition - Raumschiff.localPosition;
            double forcePower = G * Masse * RaumschiffRigidBody.mass / Mathf.Pow(forceVect.magnitude, 2);

            Debug.Log(forcePower);

            RaumschiffRigidBody.AddForce(new Vector3(forceVect.x * (float)forcePower, forceVect.y * (float)forcePower, forceVect.z * (float)forcePower));

            Debug.Log(RaumschiffRigidBody.velocity.magnitude);

            yield return null;
        }
    }
    
}
